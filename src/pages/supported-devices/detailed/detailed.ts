import { Component, ViewChild } from "@angular/core";
import { Slides, NavParams, AlertController } from "ionic-angular";

@Component({
    selector: 'page-supported-devices',
    templateUrl: './detailed.html'
})

export class DetailedPage {
    @ViewChild(Slides) slides: Slides;
    slideData = [
        { 
            image: "../../assets/slides/img1.jpg"
        },
        { 
            image: "../../assets/slides/img2.jpg"
        },
        { 
            image: "../../assets/slides/img3.jpg" 
        }];
    deviceDetail: any = {};
    constructor(
        public navParams: NavParams,
        public alertCtrl: AlertController
    ) {
        console.log("check parameters: ",navParams.get("param"));
        this.deviceDetail = navParams.get("param");
    }
    ionViewDidEnter() {
        this.slides.autoplayDisableOnInteraction = false;
    }
    purchase() {
this.alertCtrl.create({
    message: 'Do you want to proceed to payment?',
    buttons: [{
        text: 'Proceed',
        handler:() => {

        }
    },
{
    text: 'Back'
}]
}).present();
    }
}